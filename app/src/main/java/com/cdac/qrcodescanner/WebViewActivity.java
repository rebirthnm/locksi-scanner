package com.cdac.qrcodescanner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    private String locker;
    private String username = "tharchakon";
    private String name = "namo";
    private String telephone = "0123456789";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        locker = getIntent().getStringExtra("locker");
        WebView web = (WebView) findViewById(R.id.web_view);
        web.getSettings().setJavaScriptEnabled(true);
        String url = "https://locksi-locker.kisrasprint.com/loading/"+locker+"/"+username+"/"+name+"/"+telephone;

//        try {
//            Thread.sleep(100);
        web.loadUrl(url);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
